package xyz.karpador.pairandomizer.helpers

import java.io.IOException
import java.net.URL

object DownloadHelper {

    @Throws(IOException::class)
    fun downloadFileContents(uri: String): String {
        return URL(uri).openStream().bufferedReader().readText()
    }

}
