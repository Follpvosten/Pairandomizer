/*
 * Pairandomizer - An Android app for randomizing situations.
 * Copyright (C) 2018 Jonas Rinner
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package xyz.karpador.pairandomizer

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.Arrays
import java.util.Locale
import java.util.Random

import xyz.karpador.pairandomizer.data.*
import xyz.karpador.pairandomizer.exceptions.HttpResponseException
import xyz.karpador.pairandomizer.helpers.DownloadHelper

import kotlinx.serialization.json.JSON

class MainActivity : AppCompatActivity() {
    private var serverIP: String? = null

    private var serverSpecJson: Index? = null

    private var allScenarios: List<Scenario> = ArrayList()
    private var availableScenarios: List<Scenario> = ArrayList()
    private var availableLanguages: List<String> = ArrayList()
    private var currentScenario: Scenario? = null

    private val random = Random()

    private var selectedScenarioIndex = 0

    private var enabledItems: BooleanArray = BooleanArray(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        serverIP = getPreferences(Context.MODE_PRIVATE).getString(SERVER_IP_KEY, SERVER_IP_DEFAULT)

        findViewById<View>(R.id.randomizeButton).setOnClickListener { _ ->
            val text = (findViewById<View>(R.id.namesEdit) as EditText)
                    .text.toString()
            if (!text.isBlank()) {
                val names = text.split('\n')
                        .filterNot { it.isEmpty() }.toTypedArray()

                val namesList = Arrays.asList<String>(*names)
                val sceneMessage = currentScenario!!.getRandomScene(namesList, random)
                val builder = AlertDialog.Builder(this@MainActivity)
                builder.setNeutralButton(R.string.share) { _, _ ->
                    val sendIntent = Intent()
                    sendIntent.action = Intent.ACTION_SEND
                    sendIntent.putExtra(
                            Intent.EXTRA_TEXT,
                            sceneMessage.first + "\n\n" + sceneMessage.second
                    )
                    sendIntent.type = "text/plain"
                    startActivity(sendIntent)
                }
                builder.setTitle(sceneMessage.first)
                        .setMessage(sceneMessage.second)
                        .setPositiveButton(R.string.ok, null)
                        .show()
            } else {
                AlertDialog.Builder(this@MainActivity)
                        .setTitle(R.string.error)
                        .setMessage(R.string.error_no_names)
                        .setPositiveButton(R.string.ok, null)
                        .show()
            }
        }

        loadNamesIfAny()

        if (!File(filesDir, INDEX_FILE).exists()) {
            loadData()
        } else {
            try {
                serverSpecJson = JSON.parse(loadFile(INDEX_FILE))
                allScenarios = serverSpecJson!!.scenarios
                        .map {
                            Scenario(it.name, it.filename, it.lang)
                        }
                availableLanguages = serverSpecJson!!.scenarios
                        .filterNot { it.lang == null }
                        .map { it.lang!! }
                availableScenarios = getAvailableScenarios(allScenarios)
                loadScenario(getPreferences(Context.MODE_PRIVATE).getInt(CURRENT_SCENARIO_KEY, 0))
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.server_info -> {
                showServerInfo()
                return true
            }
            R.id.reload_data -> {
                loadData()
                return true
            }
            R.id.settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivityForResult(intent, SETTINGS_ACTIVITY_REQUEST)
                return true
            }
            R.id.change_scenario -> {
                showScenarioChangeDialog()
                return true
            }
            R.id.scenario_options -> {
                showScenarioOptionsDialog()
                return true
            }
            R.id.about -> {
                showAboutDialog()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onStop() {
        super.onStop()
        saveNames()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == SETTINGS_ACTIVITY_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra(SettingsActivity.SERVER_IP_CHANGED_KEY, false)) {
                    loadData()
                } else if (data.getBooleanExtra(SettingsActivity.SHOW_ALL_CHANGED_KEY, false)) {
                    availableScenarios = getAvailableScenarios(allScenarios)
                    try {
                        loadScenario(0)
                    } catch (ex: IOException) {
                        AlertDialog.Builder(this)
                                .setTitle(R.string.error)
                                .setMessage(R.string.error_loading_file)
                                .setPositiveButton(R.string.ok, null)
                                .show()
                    } catch (ex: Exception) {
                        AlertDialog.Builder(this).setTitle(R.string.error).setMessage(R.string.error_loading_file).setPositiveButton(R.string.ok, null).show()
                    }

                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun getAvailableScenarios(currentScenarios: List<Scenario>): List<Scenario> {
        if (PreferenceManager.getDefaultSharedPreferences(this)
                        .getBoolean("scenario_show_all", false))
            return currentScenarios
        val result = ArrayList<Scenario>()
        var lang = Locale.getDefault().language
        if (!availableLanguages.contains(lang))
            lang = "en"
        for (scenario in currentScenarios) {
            if (scenario.lang == null) {
                result.add(scenario)
                continue
            }
            if (scenario.lang == lang)
                result.add(scenario)
        }
        return result
    }

    private fun loadNamesIfAny() {
        if (File(filesDir, NAMES_FILE).exists()) {
            try {
                findViewById<EditText>(R.id.namesEdit).setText(loadFile(NAMES_FILE))
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun saveNames() {
        val fileContent = (findViewById<View>(R.id.namesEdit) as EditText).text.toString()
        try {
            saveFile(NAMES_FILE, fileContent)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    @Throws(IOException::class)
    private fun loadScenario(index: Int) {
        currentScenario = availableScenarios[index]
        if (!currentScenario!!.isDataLoaded) {
            val scenarioJson = loadFile(currentScenario!!.filename)
            currentScenario!!.loadJsonData(scenarioJson)
        }
        getPreferences(Context.MODE_PRIVATE).edit().putInt(CURRENT_SCENARIO_KEY, index).apply()
    }

    @Throws(IOException::class)
    private fun saveFile(filename: String, fileContent: String) {
        openFileOutput(filename, Context.MODE_PRIVATE).use {
            it.write(fileContent.toByteArray())
        }
    }

    @Throws(IOException::class)
    private fun loadFile(filename: String): String {
        return openFileInput(filename).bufferedReader().use {
            it.readText()
        }
    }

    private fun showServerInfo() {
        try {
            val message = getString(
                    R.string.server_info_message,
                    serverSpecJson!!.serverName,
                    serverSpecJson!!.comment
            )
            AlertDialog.Builder(this)
                    .setTitle(R.string.server_info)
                    .setMessage(message)
                    .setPositiveButton(R.string.ok, null)
                    .show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showScenarioChangeDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.change_scenario)
        val options = availableScenarios.map { scenario -> scenario.name }.toTypedArray()
        selectedScenarioIndex = getPreferences(Context.MODE_PRIVATE).getInt(CURRENT_SCENARIO_KEY, 0)
        builder.setSingleChoiceItems(
                options,
                selectedScenarioIndex
        ) { _, i -> selectedScenarioIndex = i }
        builder.setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok) { _, _ ->
                    try {
                        loadScenario(selectedScenarioIndex)
                        getPreferences(Context.MODE_PRIVATE).edit()
                                .putInt(CURRENT_SCENARIO_KEY, selectedScenarioIndex)
                                .apply()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
        builder.show()
    }

    private fun showScenarioOptionsDialog() {
        enabledItems = currentScenario!!.getEnabledScenes()
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.enabled_scenes)
        builder.setMultiChoiceItems(
                currentScenario!!.sceneNames,
                enabledItems
        ) { _, index, checked -> enabledItems[index] = checked }
        builder.setNegativeButton(R.string.cancel, null)
        builder.setPositiveButton(R.string.ok) { _, _ ->
            val enabledAny = enabledItems.any { enabled -> enabled }
            if (!enabledAny) {
                enabledItems[0] = true
                AlertDialog.Builder(this@MainActivity)
                        .setTitle(R.string.info)
                        .setMessage(R.string.info_all_disabled)
                        .setPositiveButton(R.string.ok, null)
                        .show()
            }
            currentScenario!!.setEnabledScenes(enabledItems)
        }
        builder.show()
    }

    private fun showAboutDialog() {
        AlertDialog.Builder(this)
                .setTitle(R.string.about_title)
                .setMessage(R.string.about_text)
                .setPositiveButton(R.string.ok, null)
                .show()
    }

    private fun loadData() {
        val progressDialog = ProgressDialog(this@MainActivity)
        progressDialog.setCancelable(false)
        progressDialog.setTitle(R.string.please_wait)
        progressDialog.setMessage(getString(R.string.loading_data))
        progressDialog.show()
        doAsync {
            var error: String? = null
            try {
                val indexFileContent = DownloadHelper.downloadFileContents("$serverIP/$INDEX_FILE")
                // Validate the file content
                serverSpecJson = JSON.parse(indexFileContent)
                // At this point, it's valid JSON, so save it
                saveFile(INDEX_FILE, indexFileContent)
                // Loop through the files the server lists and download them all
                allScenarios = serverSpecJson!!.scenarios
                        .map {
                            Scenario(it.name, it.filename, it.lang)
                        }
                for (scenario in allScenarios) {
                    val fileContent = DownloadHelper.downloadFileContents("$serverIP/${scenario.filename}")
                    // Validate JSON
                    JSON.parse<ScenarioData>(fileContent)
                    // Save valid JSON
                    saveFile(scenario.filename, fileContent)
                }
                availableLanguages = serverSpecJson!!.scenarios
                        .filterNot { it.lang == null }
                        .map { it.lang!! }
                availableScenarios = getAvailableScenarios(allScenarios)
                loadScenario(0)
            } catch (e: IOException) {
                e.printStackTrace()
                error = e.message
            } catch (e: HttpResponseException) {
                e.printStackTrace()
                error = e.message
            } catch (e: Exception) {
                e.printStackTrace()
                error = getString(R.string.error_invalid_json)
            }
            uiThread {
                progressDialog.dismiss()
                if (error != null) {
                    AlertDialog.Builder(it)
                            .setTitle(R.string.error)
                            .setMessage(error)
                            .setNeutralButton(R.string.ok, null)
                            .show()
                }
            }
        }
    }

    companion object {
        private const val SERVER_IP_KEY = "SERVER_IP"
        private const val SERVER_IP_DEFAULT = "https://karpador.xyz/pairandomizer/"
        private const val SETTINGS_ACTIVITY_REQUEST = 25566
        private const val CURRENT_SCENARIO_KEY = "CURRENT_SCENARIO"

        private const val NAMES_FILE = "names.txt"
        private const val INDEX_FILE = "index.json"
    }
}
