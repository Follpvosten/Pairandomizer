/*
 * Pairandomizer - An Android app for randomizing situations.
 * Copyright (C) 2018 Jonas Rinner
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package xyz.karpador.pairandomizer.data

import java.util.Random

import kotlinx.serialization.*
import kotlinx.serialization.json.JSON

@Serializable
data class Scene(
        val name: String,
        val messages: List<String>,
        @Optional @SerialName("triple_messages")
        val tripleMessages: List<String>? = null
) {
    fun getRandomMessage(random: Random): String {
        return messages[random.nextInt(messages.size)]
    }
    fun getRandomTriple(random: Random): String {
        return tripleMessages!![random.nextInt(tripleMessages.size)]
    }
}
@Serializable
data class ScenarioData(
        val scenes: List<Scene>,
        @SerialName("solo_messages")
        val soloMessages: List<String>
)

class Scenario(val name: String, val filename: String, val lang: String?) {
    val isDataLoaded: Boolean
        get() { return data != null }

    private var data: ScenarioData? = null
    private var enabledScenes: BooleanArray = BooleanArray(0)
    private var enabledCount = 0

    val sceneNames: Array<String>
        get() {
            return data!!.scenes.map { scene -> scene.name }.toTypedArray()
        }

    fun getEnabledScenes(): BooleanArray {
        return enabledScenes
    }

    fun setEnabledScenes(enabledScenes: BooleanArray) {
        this.enabledScenes = enabledScenes
        enabledCount = enabledScenes.count { sceneEnabled -> sceneEnabled }
    }

    fun loadJsonData(json: String) {
        data = JSON.parse(json)
        enabledScenes = BooleanArray(data!!.scenes.size) { _ -> true}
        enabledCount = enabledScenes.size
    }

    fun getRandomScene(names: MutableList<String>, random: Random): Pair<String, String> {
        names.shuffle(random)
        if(names.size == 1) {
            // Return solo message, lol
            return Pair("Forever alone...", data!!.soloMessages[random.nextInt(data!!.soloMessages.size)])
        }
        val enabled = data!!.scenes.filterIndexed { index, _ -> enabledScenes[index] }
        val scene = enabled.shuffled(random).first()
        var pairs = names.chunked(2)
        if(names.size % 2 != 0 && scene.tripleMessages != null) {
            val lastPair = pairs.takeLast(2).flatten()
            pairs = pairs.dropLast(2).plusElement(lastPair)
        }
        val result = pairs.joinToString("\n") {
            when(it.size) {
                1 -> { data!!.soloMessages[random.nextInt(data!!.soloMessages.size)].format(it[0]) }
                2 -> { scene.getRandomMessage(random).format(it[0], it[1]) }
                3 -> { scene.getRandomTriple(random).format(it[0], it[1], it[2]) }
                else -> ""
            }
        }
        return Pair(scene.name, result)
    }
}
